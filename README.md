# nutter

audio streaming experiment

## drawbacks

- very wip
- breaks easily
- only ogg mono files

## install

- libsndfile 1.0.28, 1.0.29 (prefferably 29)

```
zig build
```

## run

```
./zig-cache/bin/nutter path/to/file.ogg
```

## use

```
mpv http://localhost:8083
```
