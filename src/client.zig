const std = @import("std");
const ServerContext = @import("context.zig").ServerContext;
const utils = @import("utils.zig");

const sndfile = @import("sndfile.zig");
const c = sndfile.c;

const logger = std.log.scoped(.nutter_client);

const VirtualContext = struct {
    fd: std.os.fd_t,
    write_error: ?std.os.SendToError = null,
};

fn virtualWrite(
    data_ptr: ?*const anyopaque,
    bytes: c.sf_count_t,
    user_ptr: ?*anyopaque,
) callconv(.C) c.sf_count_t {
    const ctx = @ptrCast(*VirtualContext, @alignCast(@alignOf(VirtualContext), user_ptr));
    const data = @ptrCast([*]const u8, @alignCast(@alignOf(u8), data_ptr));
    const sliced = data[0..@intCast(usize, bytes)];

    ctx.write_error = null;
    const sent_bytes = std.os.sendto(ctx.fd, sliced, std.os.MSG.NOSIGNAL, null, 0) catch |err| {
        ctx.write_error = err;
        return @as(i64, 0);
    };

    return @intCast(i64, sent_bytes);
}

// make those functions return whatever. libsndfile wants them, even though
// it won't care about them when writing. i should fix it upstream.
fn virtualLength(_: ?*anyopaque) callconv(.C) c.sf_count_t {
    return @intCast(i64, 100000000000);
}

fn virtualSeek(offset: c.sf_count_t, whence: c_int, user_data: ?*anyopaque) callconv(.C) c.sf_count_t {
    _ = whence;
    _ = user_data;
    return offset;
}

fn virtualTell(user_data: ?*anyopaque) callconv(.C) c.sf_count_t {
    _ = user_data;
    return 0;
}

pub const Client = struct {
    ctx: *ServerContext,
    fd: std.os.fd_t,
    sock: std.net.Stream,
    address: std.net.Address,

    ready: bool = false,
    output_info: ?*c.SF_INFO = null,
    output_virtual_io: ?*c.SF_VIRTUAL_IO = null,
    output_virtual_ctx: ?*VirtualContext = null,
    output_file: ?*c.SNDFILE = null,

    const Self = @This();

    pub fn deinit(self: *Self) void {
        if (self.ready) {
            _ = c.sf_close(self.output_file.?);
            self.ctx.allocator.destroy(self.output_info.?);
            self.ctx.allocator.destroy(self.output_virtual_io.?);
            self.ctx.allocator.destroy(self.output_virtual_ctx.?);
        }
    }

    pub fn sendFrames(self: *Self, frames: []f64) !void {
        std.log.debug("fd {d} ready? {}", .{ self.fd, self.ready });
        if (!self.ready) return;

        const bytes = c.sf_writef_double(self.output_file, frames.ptr, @intCast(i64, frames.len));
        if (bytes == 0) {
            if (self.output_virtual_ctx.?.write_error) |err| return err;
            return;
        }
    }

    pub fn handleIncomingMessage(self: *Self) !void {
        var buf: [1024]u8 = undefined;

        const bytes_read = try self.sock.read(&buf);
        if (bytes_read == 0) return error.NoBytes;

        const msg = buf[0..bytes_read];
        logger.info("got msg '{s}'\n", .{msg});

        var lines = std.mem.split(u8, msg, "\r\n");
        const http_header = lines.next().?;
        var header_it = std.mem.split(u8, http_header, " ");

        const method = header_it.next() orelse {
            utils.writeHttpError(self.sock, 400, "Invalid HTTP header");
            return error.InvalidHTTP;
        };

        if (!std.mem.eql(u8, method, "GET")) {
            utils.writeHttpError(self.sock, 404, "Invalid method (only GET)");
            return error.InvalidHTTP;
        }

        const path = header_it.next() orelse {
            logger.info("msg: '{s}'\n", .{msg});
            utils.writeHttpError(self.sock, 404, "Wanted path, got nothing");
            return error.InvalidHTTP;
        };

        if (!std.mem.eql(u8, path, "/")) {
            logger.info("msg: '{s}'\n", .{msg});
            var msg_buf: [1024]u8 = undefined;
            var err_msg = try std.fmt.bufPrint(&msg_buf, "Invalid path (only /, got {s})", .{path});
            utils.writeHttpErrorRuntime(self.sock, 404, err_msg);
            return error.InvalidHTTP;
        }

        const http_flag = header_it.next() orelse {
            utils.writeHttpError(self.sock, 400, "Invalid HTTP header");
            return error.InvalidHTTP;
        };

        if (!std.mem.eql(u8, http_flag, "HTTP/1.1")) {
            utils.writeHttpError(self.sock, 400, "Invalid HTTP version (only 1.1 accepted)");
            return error.InvalidHTTP;
        }

        logger.info("got http request from {}\n", .{self.address});

        // send out initial message which should trigger the client to keep waiting
        // TODO proper content-type by libmagic

        const reply =
            "HTTP/1.1 200 OK\r\n" ++
            "content-type: audio/wav\r\n" ++
            "access-control-allow-origin: *\r\n" ++
            "server: nutter\r\n\r\n";
        const sent_bytes = try std.os.send(self.sock.handle, reply, 0);

        if (sent_bytes < reply.len) logger.info("warn: wanted to send {d} but sent {d} bytes", .{ reply.len, sent_bytes });

        const upstream_info = self.ctx.sndfile.info;
        self.output_info = try self.ctx.allocator.create(c.SF_INFO);
        self.output_info.?.* = c.SF_INFO{
            .frames = upstream_info.frames,
            .samplerate = upstream_info.samplerate,
            .channels = upstream_info.channels,
            .format = upstream_info.format,
            .sections = upstream_info.sections,
            .seekable = 0,
        };

        logger.info("{any}\n", .{self.output_info});
        self.output_virtual_io = try self.ctx.allocator.create(c.SF_VIRTUAL_IO);
        self.output_virtual_io.?.* = c.SF_VIRTUAL_IO{
            .get_filelen = virtualLength,
            .seek = virtualSeek,
            .read = null,
            .write = virtualWrite,
            .tell = virtualTell,
        };
        self.output_virtual_ctx = try self.ctx.allocator.create(VirtualContext);
        self.output_virtual_ctx.?.* = VirtualContext{ .fd = self.sock.handle };

        self.output_file = c.sf_open_virtual(
            self.output_virtual_io.?,
            @enumToInt(sndfile.Mode.Write),
            @ptrCast(*c.SF_INFO, self.output_info.?),
            @ptrCast(*anyopaque, self.output_virtual_ctx.?),
        );

        const status = c.sf_error(self.output_file.?);
        if (status != 0) {
            const err = std.mem.span(c.sf_error_number(status));
            logger.info("failed to open write fd {} ({s})\n", .{ self.sock.handle, err });
            utils.writeHttpErrorRuntime(self.sock, 500, err);

            var logbuf: [10 * 1024]u8 = undefined;
            const count = c.sf_command(self.output_file.?, c.SFC_GET_LOG_INFO, &logbuf, 10 * 1024);
            const msgs = logbuf[0..@intCast(usize, count)];
            logger.info("libsndfile log {s}\n", .{msgs});

            return error.SndFileFail;
        }

        self.ready = true;

        return;
    }
};
