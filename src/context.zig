const std = @import("std");
const Client = @import("client.zig").Client;

const ClientMap = std.AutoHashMap(std.os.fd_t, Client);
const SoundFile = @import("sndfile.zig").SoundFile;

pub const ServerContext = struct {
    allocator: std.mem.Allocator,
    client_map_lock: std.Thread.Mutex = .{},
    client_map: ClientMap,
    sndfile: *SoundFile,

    const Self = @This();

    pub fn init(allocator: std.mem.Allocator, sndfile: *SoundFile) Self {
        return .{
            .allocator = allocator,
            .client_map = ClientMap.init(allocator),
            .sndfile = sndfile,
        };
    }

    pub fn deinit(self: *Self) void {
        // TODO do we acquire the lock here?
        self.client_map.deinit();
    }

    pub fn newClient(self: *Self, client_fd: std.os.fd_t, addr: std.net.Address) !void {
        self.client_map_lock.lock();
        defer self.client_map_lock.unlock();

        try self.client_map.put(
            client_fd,
            Client{
                .ctx = self,
                .fd = client_fd,
                .sock = std.net.Stream{ .handle = client_fd },
                .address = addr,
            },
        );
    }

    pub fn removeClient(self: *Self, client_fd: std.os.fd_t) void {
        self.client_map_lock.lock();
        defer self.client_map_lock.unlock();

        _ = self.client_map.remove(client_fd);
    }
};
