const std = @import("std");

const sndfile = @import("sndfile.zig");
const ServerContext = @import("context.zig").ServerContext;
const utils = @import("utils.zig");
const Client = @import("client.zig").Client;

const PollFdList = std.ArrayList(std.os.pollfd);

fn createSignalFd() !std.os.fd_t {
    var mask: std.os.sigset_t = undefined;

    // maybe just std.mem.set with 0 but /shrug
    for (&mask) |*value| {
        value.* = 0;
    }

    std.os.linux.sigaddset(&mask, std.os.SIG.TERM);
    std.os.linux.sigaddset(&mask, std.os.SIG.INT);

    _ = std.os.linux.sigprocmask(std.os.SIG.BLOCK, &mask, null);
    // mask[20] = 16386;

    return try std.os.signalfd(-1, &mask, 0);
}

fn readFromSignalFd(signal_fd: std.os.fd_t) !void {
    var buf: [@sizeOf(std.os.linux.signalfd_siginfo)]u8 align(8) = undefined;
    _ = try std.os.read(signal_fd, &buf);

    var siginfo = @ptrCast(*std.os.linux.signalfd_siginfo, @alignCast(
        @alignOf(*std.os.linux.signalfd_siginfo),
        &buf,
    ));

    var sig = siginfo.signo;
    if (sig != std.os.SIG.INT and sig != std.os.SIG.TERM) {
        logger.info("got signal {}, not INT ({}) or TERM ({}), ignoring", .{
            sig,
            std.os.SIG.INT,
            std.os.SIG.TERM,
        });

        return;
    }

    logger.info("got SIGINT or SIGTERM, stopping!", .{});
    return error.Shutdown;
}

fn handleNewClient(
    ctx: *ServerContext,
    server: *std.net.StreamServer,
    sockets: *PollFdList,
) void {
    var conn = server.accept() catch |err| {
        logger.info("Failed to accept: {}", .{err});
        return;
    };

    var sock = conn.stream;
    logger.info("got client (addr={}) at fd={d}\n", .{ conn.address, sock.handle });

    ctx.newClient(sock.handle, conn.address) catch {
        utils.writeHttpError(sock, 500, "out of memory");
        return;
    };

    sockets.append(std.os.pollfd{
        .fd = sock.handle,
        .events = std.os.POLL.IN,
        .revents = 0,
    }) catch {
        utils.writeHttpError(sock, 500, "out of memory");
        return;
    };
}

fn readerThread(ctx: *ServerContext) !void {
    const frames_per_sec = ctx.sndfile.info.samplerate;
    var buf = try ctx.allocator.alloc(f64, @intCast(usize, frames_per_sec));
    defer ctx.allocator.free(buf);

    var failed_clients = std.ArrayList(std.os.fd_t).init(ctx.allocator);
    defer failed_clients.deinit();

    while (true) {
        const read_frames = ctx.sndfile.read(buf);
        std.debug.assert(read_frames > 0);
        logger.info("read {d} frames\n", .{read_frames});

        // hold the mutex only for the sending to clients. we do not want to
        // lock on the sleep(1) as that would block every single connection unless
        // you had really good timing.
        {
            ctx.client_map_lock.lock();
            defer ctx.client_map_lock.unlock();

            var iter = ctx.client_map.iterator();
            while (iter.next()) |entry| {
                const client = entry.value_ptr;
                client.sendFrames(buf) catch |err| {
                    // TODO this will DEADLOCK
                    logger.info("Client at fd {} errored: {}\n", .{ client.fd, err });
                    try failed_clients.append(client.fd);
                };
            }
        }

        while (true) {
            const maybe_client_fd = failed_clients.popOrNull();
            if (maybe_client_fd == null) break;
            const client_fd = maybe_client_fd.?;

            var client: Client = undefined;
            {
                ctx.client_map_lock.lock();
                defer ctx.client_map_lock.unlock();
                client = ctx.client_map.get(client_fd).?;
            }
            client.ready = false;
            {
                ctx.client_map_lock.lock();
                defer ctx.client_map_lock.unlock();

                ctx.removeClient(client.fd);
            }

            client.deinit();
            std.os.close(client.fd);
            //_ = sockets.orederedRemove(index);
        }

        std.time.sleep(1 * std.time.ns_per_s);
    }
}

const logger = std.log.scoped(.nutter);

pub fn main() anyerror!void {
    var args = std.process.args();
    _ = args.skip();
    const audio_file_path = args.next() orelse @panic("failed to get audio file path");
    logger.warn("opening audio file at '{s}'\n", .{audio_file_path});

    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        _ = allocator_instance.deinit();
    }
    const allocator = allocator_instance.allocator();

    var info = std.mem.zeroes(sndfile.Info);
    var file = try sndfile.SoundFile.open(allocator, audio_file_path, sndfile.Mode.Read, &info);
    defer file.close();
    logger.info("{}\n", .{info});

    // TODO: check on multi-channel files (only mono works now)

    var ctx = ServerContext.init(allocator, &file);

    const thread = try std.Thread.spawn(.{}, readerThread, .{&ctx});
    defer thread.detach();

    var sockets = PollFdList.init(allocator);
    defer sockets.deinit();

    const signal_fd = try createSignalFd();
    defer std.os.close(signal_fd);

    try sockets.append(std.os.pollfd{
        .fd = signal_fd,
        .events = std.os.POLL.IN,
        .revents = 0,
    });

    // open socket to serve that audio file
    var server = std.net.StreamServer.init(.{ .reuse_address = true });
    defer server.deinit();

    var addr = try std.net.Address.resolveIp("127.0.0.1", 8083);
    try server.listen(addr);

    try sockets.append(std.os.pollfd{
        .fd = server.sockfd.?,
        .events = std.os.POLL.IN,
        .revents = 0,
    });

    while (true) {
        var pollfds = sockets.items;
        logger.info("polling {} sockets...", .{pollfds.len});
        const available = try std.os.poll(pollfds, -1);

        // shouldn't happen, but it could.
        if (available == 0) continue;

        for (pollfds) |pollfd, index| {
            if (pollfd.revents == 0) continue;

            if (pollfd.fd == signal_fd) {
                readFromSignalFd(signal_fd) catch |err| {
                    if (err == error.Shutdown) return else logger.warn("failed to read from signal fd: {}\n", .{err});
                };
            } else if (pollfd.fd == server.sockfd.?) {
                // new client!
                handleNewClient(&ctx, &server, &sockets);
            } else {
                // a client has sent us data
                var client: *Client = undefined;
                {
                    ctx.client_map_lock.lock();
                    defer ctx.client_map_lock.unlock();
                    client = ctx.client_map.getPtr(pollfd.fd).?;
                }

                client.handleIncomingMessage() catch |err| {
                    logger.warn("Client at fd {} errored: {}\n", .{ pollfd.fd, err });
                    ctx.removeClient(client.fd);
                    client.deinit();
                    std.os.close(client.fd);
                    _ = sockets.orderedRemove(index);
                };
            }
        }
    }
}
