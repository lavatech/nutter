const std = @import("std");

const logger = std.log.scoped(.http);

pub fn writeHttpError(
    sock: std.net.Stream,
    comptime status_code: u32,
    comptime message: []const u8,
) void {
    comptime var buf: [1024]u8 = undefined;
    const response = comptime blk: {
        break :blk try std.fmt.bufPrint(
            &buf,
            "HTTP/1.1 {}\r\ncontent-type: text/plain\r\ncontent-length: {}\r\n\r\n{s}",
            .{
                status_code,
                message.len,
                message,
            },
        );
    };

    logger.info("error: {} {s}\n", .{ status_code, message });

    _ = sock.write(response) catch |err| {
        logger.info("Failed to send error '{s}' to fd {}: {}\n", .{
            message,
            sock.handle,
            err,
        });
    };
}

pub fn writeHttpErrorRuntime(
    sock: std.net.Stream,
    status_code: u32,
    message: []const u8,
) void {
    var buf: [1024]u8 = undefined;
    const response = std.fmt.bufPrint(
        &buf,
        "HTTP/1.1 {}\r\ncontent-type: text/plain\r\ncontent-length: {}\r\n\r\nnutter error: {s}",
        .{
            status_code,
            message.len,
            message,
        },
    ) catch unreachable;

    logger.info("error: {} {s}\n", .{ status_code, message });

    _ = sock.write(response) catch |err| {
        logger.info("Failed to send error '{s}' to fd {}: {}\n", .{
            message,
            sock.handle,
            err,
        });
    };
}
